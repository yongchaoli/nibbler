﻿using NibblerBackend;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NibblerUnitTests
{
    [TestClass]
    public class PointTest
    {
        [TestMethod]
        public void TestTwoPoints()
        {
            Point p1 = new Point(1, 2);
            Point p2 = new Point(1, 2);
            Point p3 = new Point(1, 3);
            Assert.IsTrue(p1.Equals(p2));
            Assert.IsFalse(p2.Equals(p3));
            Assert.IsTrue(p1.GetHashCode() == p2.GetHashCode());
        }
    }
}
