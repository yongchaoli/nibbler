﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NibblerBackend;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Nibbler
{
    //skeleton class code the logic:)
    public class CaterpillarSprite: DrawableGameComponent
    {
        private GameState gameState;
        //to render
        private SpriteBatch spriteBatch;
        private Texture2D caterpillarIMG;
        private Texture2D good;
        private SpriteFont font;
        private Texture2D tokenImg;
        private Texture2D wallImg;
        private Texture2D floorImg;
        private Game1 game;
        //keyboard input
        private KeyboardState oldState;
        private int counter;
        private int threshold;
        private ChangeDirectionEventArgs changeDirectionEventArgs;

        private char[,] map;

        public CaterpillarSprite(Game1 game) : base(game)
        {
            //To do logic
            this.game = game;
            //cat = caterpillar;
            gameState = new GameState();
            gameState.Load("Content/sampleGrid.txt");
            map = gameState.Update();
            changeDirectionEventArgs = new ChangeDirectionEventArgs();
        }
        public override void Initialize()
        {
            
            threshold = 18; //idk we can change threshold later on 
            //add logic before base.----
            base.Initialize();
        }
        protected override void LoadContent()
        {
            //rendering
            spriteBatch = new SpriteBatch(GraphicsDevice); //NEEDED whenever we have to draw to the screen
            caterpillarIMG = game.Content.Load<Texture2D>("purpur_block");
            floorImg = game.Content.Load<Texture2D>("prismarine_bricks");
            wallImg = game.Content.Load<Texture2D>("tnt_bottom");
            tokenImg = game.Content.Load<Texture2D>("redstone_block");
            good = game.Content.Load<Texture2D>("slime");
            
            font = game.Content.Load<SpriteFont>("fonts/arial-24");
            //add logic before base.----
            base.LoadContent();
        }
        public override void Update(GameTime gameTime)
        {
            //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            //1.)   Change Direction
            //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            //KeyboardState newState = Keyboard.GetState();
            oldState = Keyboard.GetState();
            

            if (oldState.IsKeyDown(Keys.Right))
            {
                changeDirectionEventArgs.ChangeTo = Direction.RIGHT;
            }
            else if (oldState.IsKeyDown(Keys.Left))
            {
                changeDirectionEventArgs.ChangeTo = Direction.LEFT;
            }
            else if (oldState.IsKeyDown(Keys.Up))
            {
                changeDirectionEventArgs.ChangeTo = Direction.UP;
            }
            else if (oldState.IsKeyDown(Keys.Down))
            {
                changeDirectionEventArgs.ChangeTo = Direction.DOWN;
            }
            else if (oldState.IsKeyDown(Keys.Enter))
            {
                gameState.Load("Content/sampleGrid.txt");
                return;
            }

            

            gameState.OnChangeDirection(changeDirectionEventArgs);


            if (counter > threshold)
            {

                //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                //2.)   Update Caterpillar's position
                //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

                map = gameState.Update();
                counter = 0;
            }
            else
            {
                counter++;
            }

            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            //add logic before base.----
            spriteBatch.Begin();

            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (map[i,j] == 'C')
                    {
                        spriteBatch.Draw(caterpillarIMG, new Vector2(i * 32, j * 32), Color.White);
                    }
                    else if (map[i, j] == 'W')
                    {
                        spriteBatch.Draw(wallImg, new Vector2(i * 32, j * 32), Color.White);
                    }
                    else if (map[i, j] == 'G')
                    {
                        spriteBatch.Draw(tokenImg, new Vector2(i * 32, j * 32), Color.White);
                    }
                    else if (map[i, j] == 'S')
                    {
                        spriteBatch.Draw(good, new Vector2(i * 32, j * 32), Color.White);
                    }
                    else
                    {
                        spriteBatch.Draw(floorImg, new Vector2(i * 32, j * 32), Color.White);
                    }
                }

            }

            spriteBatch.DrawString(font, "Score: " + gameState.Score, new Vector2(10, Game1.sizeValue * 10), Color.Black);
            spriteBatch.DrawString(font, "Lives: " + gameState.Lives, new Vector2(150, Game1.sizeValue * 10), Color.Black);

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
