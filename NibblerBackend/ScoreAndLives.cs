﻿using System;

namespace NibblerBackend
{
    public class ScoreAndLives
    {
        public int Score { get; set; }
        public int Lives { get; set; }
        public event EventHandler<EventArgs> NoMoreLives;

        public void CaterpillarDies(object sender, EventArgs e)
        {
            Lives--;

            if (Lives == 0)
            {
                NoMoreLives?.Invoke(this, new EventArgs());
            }
        }

        public void AddPointsAndLives(object sender, EventArgs e)
        {
            if (sender is ICollidable)
            {
                Score += ((ICollidable)sender).Points;
            }
             
        }
    }
}
