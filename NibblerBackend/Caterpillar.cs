﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NibblerBackend
{
    public class Caterpillar
    {
        private Queue<Point> _tiles;
        private Direction _direction;
        private Point _frontElement;
        private Grid _grid;

        public event EventHandler<EventArgs> ResetEvent;
        
        public IEnumerable<Point> Points
        {
            get
            {
                return _tiles;
            }
        }
        
        public Caterpillar(Grid grid)
        {
            _tiles = new Queue<Point>();
            _direction = Direction.LEFT;

            
            int startX = grid.Width / 2 ;
            int startY = grid.Height / 2;
            
            //for (int i = 0; i < 4; i++)
            //{
                _tiles.Enqueue(new Point(startX, startY));
            //    startX++;
            //}

            _frontElement = _tiles.Last();
            
            _grid = grid;
        }

        public void ChangeDirection(object sender, ChangeDirectionEventArgs e)
        {
            if (Math.Abs(_direction - e.ChangeTo) == 2)
            {
                ReversePoint();
                _direction = e.ChangeTo;
            }
            else
            {
                _direction = e.ChangeTo;
            }            
        }

        private void ReversePoint()
        {
            Queue<Point> reverseQueue = new Queue<Point>();            

            for (int i = Points.Count() - 1; i >= 0; i--)
            {
                reverseQueue.Enqueue(Points.ElementAt(i));                
            }

            _tiles = reverseQueue;

            _frontElement = _tiles.Last();

        }

        public void Update()
        {
            Tuple<int, int> components = GetComponents();

            Point nextPoint = new Point(_frontElement.X + components.Item1, _frontElement.Y + components.Item2);

            if (Contains(nextPoint))
            {
                Reset();
                return;
            }

            _tiles.Dequeue();
            _tiles.Enqueue(nextPoint);
            _frontElement = _tiles.Last();
            ICollidable collidable = _grid[_frontElement.X, _frontElement.Y];



            if (collidable != null)
            {
                collidable.Collide(this);
            }
        }

        private Tuple<int, int> GetComponents()
        {
            if (_direction == Direction.RIGHT)
            {
                return Tuple.Create(1, 0);
            }
            else if (_direction == Direction.UP)
            {
                return Tuple.Create(0, -1);
            }
            else if (_direction == Direction.LEFT)
            {
                return Tuple.Create(-1, 0);
            }
            else
            {
                return Tuple.Create(0, 1);
            }
        }

        public void Grow(int growLength)
        {
            Point tail = _tiles.Peek();
            var items = _tiles.ToArray();
            _tiles.Clear();           

            for (int i = 0; i < growLength; i++)
            {
                _tiles.Enqueue(tail);
            }
            foreach (var item in items)
            {
                _tiles.Enqueue(item);
            }
        }

        public void Reset()
        {
            int startX = _grid.Width / 2;
            int startY = _grid.Height / 2;

            Queue<Point> reset = new Queue<Point>();

            foreach (var item in Points)
            {
                reset.Enqueue(new Point(startX, startY));
            }
            _tiles = reset;
            _frontElement = _tiles.Last();

            ResetEvent?.Invoke(this, new EventArgs());
        }

        public void Shrinking(int shrinkingTime)
        {
            while (_tiles.Count > 1 && shrinkingTime > 0)
            {
                _tiles.Dequeue();
                shrinkingTime--;
            }
        }

        private bool Contains(Point p)
        {
            bool contain = false;

            foreach (var item in _tiles)
            {
                if (p.X == item.X && p.Y == item.Y)
                {
                    contain = true;
                    break;
                }
            }

            return contain;
        }

        public void OnCheck(object sender, CheckEventArgs e)
        {
            if (sender is Grid)
            {
                Grid grid = (Grid)sender;

                grid.SpaceIsOK = true;                

                if (Contains(e.Place))
                {
                    grid.SpaceIsOK = false;
                }
                
            }
        }
    }

}
