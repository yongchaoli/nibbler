﻿using System;
using System.Collections.Generic;
using System.IO;


namespace NibblerBackend
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Press up/down/left/right arrow to play, q for quit.");

            //Console.WriteLine(Directory.GetCurrentDirectory());

            GameState gameState = new GameState();
            gameState.DisplayScore += DisplayScore;
            //Console.WriteLine(gameState.Load(@"E:\Dawson\2019winter\dot net with c sharp\sampleGrid.txt"));
            if (gameState.Load(@Directory.GetCurrentDirectory() + @"\sampleGrid.txt") != "succee")
            {
                System.Environment.Exit(1);
            }

            ChangeDirectionEventArgs e = new ChangeDirectionEventArgs();


            char[,] map = gameState.Update();

            ConsoleKeyInfo userInput;

            do
            {
                if (!gameState.ShouldContinue)
                {
                    break;
                }
                
                
                Console.WriteLine();
                DisplayArray(map);
                Console.WriteLine();
                userInput = Console.ReadKey();
                switch (userInput.Key)
                {
                    case ConsoleKey.RightArrow:
                        e.ChangeTo = Direction.RIGHT;
                        break;
                    case ConsoleKey.LeftArrow:
                        e.ChangeTo = Direction.LEFT;
                        break;
                    case ConsoleKey.DownArrow:
                        e.ChangeTo = Direction.DOWN;
                        break;
                    case ConsoleKey.UpArrow:
                        e.ChangeTo = Direction.UP;
                        break;
                }
                gameState.OnChangeDirection(e);
                map = gameState.Update();
            }
            while (userInput.KeyChar != 'q' );

            Console.ReadKey();
        }

        public static void DisplayArray(char[,] display)
        {
            for (int j = 0; j < display.GetLength(1); j++) 
            {
                for (int i = 0; i < display.GetLength(0); i++)
                {
                    Console.Write(display[i, j]);
                }
                Console.WriteLine();
            }
        }

        public static void DisplayScore(object sender, EventArgs e)
        {
            Console.WriteLine(((GameState)sender).GetScore());
            //System.Environment.Exit(1);
        }
    }
}
