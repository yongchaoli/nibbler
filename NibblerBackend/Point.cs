﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NibblerBackend
{
    public class Point
    {
        private int _x;
        private int _y;

        public int X { get { return this._x; } }
        public int Y { get { return this._y; } }

        public Point(int x, int y)
        {
            this._x = x;
            this._y = y;
        }

        public override bool Equals(object comparingToPoint)
        {
            if (comparingToPoint == null || !(comparingToPoint is Point))
            {
                return false;
            }

            var point = (Point)comparingToPoint;

            return this._x == point.X && this._y == point.Y;            
        }

        public override int GetHashCode()
        {
            return (17 + 23 * X.GetHashCode()) * 23 * Y.GetHashCode();
        }
    }
}
