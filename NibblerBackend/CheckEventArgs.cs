﻿using System;

namespace NibblerBackend
{
    public class CheckEventArgs : EventArgs
    {
        public Point Place { get; set; }
    }
}
