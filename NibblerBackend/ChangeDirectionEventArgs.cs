﻿using System;

namespace NibblerBackend
{
    public class ChangeDirectionEventArgs : EventArgs 
    {
        public Direction ChangeTo { get; set; }
    }
}
