﻿using System;

namespace NibblerBackend
{
    public class CaterpillerShrinker : ICollidable
    {
        public int Points { get; set; }
        public int NumLivesGained { get; set ; }
        public int NumNewTokens { get; set ; }
        public ShrinkingDistribution ShrinkingDistribution { get; set; }

        public double Possibility { get; set; }


        public event EventHandler<EventArgs> Collision;

        public void Collide(Caterpillar c)
        {
            int shrinkingTimes = 0;


            if (Possibility < ShrinkingDistribution.DecreasingByOne)
            {
                shrinkingTimes = 1;
            }
            else
            {
                shrinkingTimes = 2;
            }

            c.Shrinking(shrinkingTimes);

            Collision?.Invoke(this, new EventArgs());

 
        }
    }
}
