﻿namespace NibblerBackend
{
    public class GrowTokenDistribution
    {
        public double IncreasingByOnePercentage { get; set; }
        public double IncreasingByTwoPercentage { get; set; }
        public double IncreasingByFivePercentage { get; set; }
    }
}
