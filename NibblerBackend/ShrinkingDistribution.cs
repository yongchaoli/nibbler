﻿namespace NibblerBackend
{
    public class ShrinkingDistribution
    {
        public double DecreasingByOne { get; set; }
        public double DecreasingByTwo { get; set; }
    }
}
