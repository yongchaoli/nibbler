﻿using System;

namespace NibblerBackend
{
    public class Wall : ICollidable
    {
        public int NumLivesGained { get; set; }
        public int NumNewTokens { get; set; }
        public int Points { get; set; }

        public event EventHandler<EventArgs> Collision;

        public void Collide(Caterpillar c)
        {
            c.Reset();
            Collision?.Invoke(c, new EventArgs());
        }

    }
}
