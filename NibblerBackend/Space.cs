﻿using System;

namespace NibblerBackend
{
    public class Space : ICollidable
    {
        public int Points { get; set; }
        public int NumLivesGained { get; set; }
        public int NumNewTokens { get; set; }

        public event EventHandler<EventArgs> Collision;

        public void Collide(Caterpillar c)
        {
            
        }
    }
}
