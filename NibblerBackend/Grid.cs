﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NibblerBackend
{
    public class Grid
    {
        private ICollidable[,] _collidables;

        public bool SpaceIsOK { get; set; }

        public double GoodTokenPossibility { get; set; }

        public GrowTokenDistribution GrowTokenDistribution { get; set; }

        public ShrinkingDistribution ShringkingDistribution { get; set; }

        public Point Token { get; set; }

        public int Width { get { return _collidables.GetLength(0); } }

        public int Height { get { return _collidables.GetLength(1); } }

        private double[] _possibilities;

        public event EventHandler<CheckEventArgs> CheckSpace;

        public event EventHandler<EventArgs> PopupScore;

        public Grid(ICollidable[,] collidables)
        {
            _collidables = collidables;

            Token = new Point(-1, -1);

            _possibilities = new double[100000];

            Random random = new Random(Guid.NewGuid().GetHashCode());

            for (int i = 0; i < _possibilities.Length; i++)
            {
                _possibilities[i] = random.NextDouble();
            }


        }

        public ICollidable this[int x, int y]
        {
            get
            {
                if (x >=0 && x < _collidables.GetLength(0) && y >= 0 && y < _collidables.GetLength(1))
                {
                    return _collidables[x, y];
                }
                else
                {
                    return null;
                }
                
            }

            set
            {
                if (value is ICollidable)
                {
                    _collidables[x, y] = value;
                }                
            }
        }

        public void AddNextTokens(object sender, EventArgs e)
        {
            ICollidable collidable = null;

            if (Token.X > 0 && Token.X < Width - 1 && Token.Y > 0 && Token.Y < Height - 1)
            {
                for (int i = 0; i < Width; i++)
                {
                    for (int j = 0; j < Height; j++)
                    {
                        if (this[i,j] is Space)
                        {
                            this[Token.X, Token.Y] = this[i, j];
                            break;
                        }
                    }
                }
            }

            Random random = new Random(Guid.NewGuid().GetHashCode());            

            double possibility = _possibilities[random.Next(100000)];            

            int x = 0, y = 0;

            do
            {
                x = random.Next(Width - 3) + 1;
                y = random.Next(Height - 3) + 1;

                CheckEventArgs checkEventArgs = new CheckEventArgs();

                checkEventArgs.Place = new Point(x, y);

                CheckSpace?.Invoke(this, checkEventArgs);
            }
            while (!SpaceIsOK);

            SpaceIsOK = false;




            if (possibility <= GoodTokenPossibility)
            {
                CaterpillerShrinker shrinking = new CaterpillerShrinker();
                shrinking.ShrinkingDistribution = ShringkingDistribution;
                shrinking.Possibility = _possibilities[random.Next(100000)];
                
                shrinking.Points = random.Next(100);
                shrinking.Collision += AddNextTokens;
                collidable = shrinking;                
            }
            else
            {
                CaterpillarGrower grow = new CaterpillarGrower();
                grow.GrowTokenDistribution = GrowTokenDistribution;
                grow.Possibility = _possibilities[random.Next(100000)];
                
                grow.Points = random.Next(100);
                grow.Collision += AddNextTokens;
                collidable = grow;                
            }

            collidable.Collision += PopupScore;

            this[x, y] = collidable;

            Token = new Point(x,y);
        }
    }
}
