﻿using System;

namespace NibblerBackend
{
    public class CaterpillarGrower : ICollidable
    {
        public int Points { get; set; }
        public int NumLivesGained { get; set; }
        public int NumNewTokens { get; set; }
        public GrowTokenDistribution GrowTokenDistribution { get; set; }
        public double Possibility { get; set; }
        public event EventHandler<EventArgs> Collision;

        public void Collide(Caterpillar c)
        {
            int growLength = 0;

            if (Possibility <= GrowTokenDistribution.IncreasingByOnePercentage)
            {
                growLength = 1;
            }
            else if (Possibility <= GrowTokenDistribution.IncreasingByOnePercentage + GrowTokenDistribution.IncreasingByTwoPercentage)
            {
                growLength = 2;
            } 
            else
            {
                growLength = 5;
            }
            
            c.Grow(growLength);

            Collision?.Invoke(this, new EventArgs());
        }
    }
}
