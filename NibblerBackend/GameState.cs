﻿using System;
using System.IO;

namespace NibblerBackend
{
    public class GameState
    {
        private Grid _grid;
        
        private Wall _wall;
        private Space _space;
        private ScoreAndLives _scoreAndLives;
        private Caterpillar _catepillar;
        private double _goodTokenGenPercentage;

        public event EventHandler<EventArgs> DisplayScore;

        public bool ShouldContinue { get; set; }

        private event EventHandler<ChangeDirectionEventArgs> ChangeDirection; 

        public int Score
        {
            get
            {
                return _scoreAndLives.Score;
            }
        }

        public int Lives
        {
            get
            {
                return _scoreAndLives.Lives;
            }
        }

        public GameState()
        {

            _wall = new Wall();

            
            _space = new Space();
            
            
            
        }

        public string Load(string fileName)
        {
            string loadStatus = "succee";

            string[] fileContent = null;

            ShouldContinue = true;

            try
            {
                fileContent = File.ReadAllLines(@fileName);
                
                if (fileContent.Length <= 3)
                {
                    return loadStatus = "File content is not correct.";
                }

                char[] leftBoarder = new char[fileContent.Length - 5];
                char[] topBoarder = fileContent[3].ToUpper().ToCharArray();
                char[] rightBoarder = new char[fileContent.Length - 5];
                char[] bottomBoarder = fileContent[fileContent.Length-1].ToUpper().ToCharArray();

                if (topBoarder.Length != bottomBoarder.Length)
                {
                    return loadStatus = "Not a rectangular map";
                }

                for (int i = 4, lineSize = fileContent[3].Length, indexOfBoarder = 0; i < fileContent.Length - 1; i++, indexOfBoarder++)
                {
                    if (lineSize != fileContent[i].Length || fileContent[i].Length < 3)
                    {
                        return loadStatus = "Not a rectangular map";
                    }
                    
                    leftBoarder[indexOfBoarder] = fileContent[i].Substring(0, 1).ToUpper().ToCharArray()[0];
                    rightBoarder[indexOfBoarder] = fileContent[i].Substring(fileContent[i].Length - 1, 1).ToUpper().ToCharArray()[0];                    
                }

                for (int i = 0; i < topBoarder.Length; i++)
                {
                    if (topBoarder[i] != 'W' || bottomBoarder[i] != 'W')
                    {
                        return loadStatus = "Boarder is not correct.";
                    }
                }

                for (int i = 0; i < leftBoarder.Length; i++)
                {
                    if (leftBoarder[i] != 'W' || rightBoarder[i] != 'W')
                    {
                        return loadStatus = "Boarder is not correct.";
                    }
                }

            }
            catch (FileNotFoundException e)
            {                
                return loadStatus = "File \"" + fileName + "\" not found.";
            }
            catch (SystemException e)
            {
                return loadStatus = e.ToString();
            }

            if (loadStatus == "succee")
            {
                GrowTokenDistribution badTokenDistribution = new GrowTokenDistribution();
                ShrinkingDistribution stringkingDistribution = new ShrinkingDistribution();

                if (!double.TryParse(fileContent[0], out _goodTokenGenPercentage))
                {

                    return loadStatus = "Geting token percentage data error.";
                }
                               
                string[] tokenData = fileContent[1].Split(' ');
                double increasingByOne, increasingByTwo, increasingByFive;

                if (!double.TryParse(tokenData[0], out increasingByOne) || !double.TryParse(tokenData[1], out increasingByTwo) || !double.TryParse(tokenData[2], out increasingByFive))
                {
                    return loadStatus = "Geting token percentage data error.";
                }
                               
                badTokenDistribution.IncreasingByOnePercentage = increasingByOne;
                badTokenDistribution.IncreasingByTwoPercentage = increasingByTwo;
                badTokenDistribution.IncreasingByFivePercentage = increasingByFive;

                if ((badTokenDistribution.IncreasingByOnePercentage + badTokenDistribution.IncreasingByTwoPercentage + badTokenDistribution.IncreasingByFivePercentage) != 1.0)
                {
                    return loadStatus = "Geting token percentage data for bad token assignment error.";
                }

                tokenData = fileContent[2].Split(' ');
                double decreasingByOne, decreasingByTwo;

                if (!double.TryParse(tokenData[0], out decreasingByOne) || !double.TryParse(tokenData[1], out decreasingByTwo))
                {
                    return loadStatus = "Geting token percentage data error.";
                }

                stringkingDistribution.DecreasingByOne = decreasingByOne;
                stringkingDistribution.DecreasingByTwo = decreasingByTwo;

                if ((stringkingDistribution.DecreasingByOne + stringkingDistribution.DecreasingByTwo) != 1.0)
                {
                    return loadStatus = "Geting token percentage data for shringking token assignment error.";
                }

                char[] lineData;

                ICollidable[,] collidables = new ICollidable[fileContent[3].Length, fileContent.Length - 3];

                for (int i = 0; i < fileContent.Length - 3; i++)
                {
                    lineData = fileContent[i + 3].ToUpper().ToCharArray();

                    for (int j = 0; j < lineData.Length; j++)
                    {
                        if (lineData[j] == 'W')
                        {
                            collidables[j, i] = _wall;
                        }
                        else
                        {
                            collidables[j, i] = _space;
                        }
                    }

                }

                //for (int i = 0; i < collidables.GetLength(0); i++)
                //{
                //    for (int j = 0; j < collidables.GetLength(1); j++)
                //    {
                //        if (collidables[i,j] is Wall)
                //        {
                //            Console.Write('W');
                //        }
                //        else
                //        {
                //            Console.Write(' ');
                //        }
                //    }
                //    Console.WriteLine();
                //}

                _grid = new Grid(collidables);
                _grid.GoodTokenPossibility = _goodTokenGenPercentage;
                _grid.GrowTokenDistribution = badTokenDistribution;
                _grid.ShringkingDistribution = stringkingDistribution;

               

                _catepillar = new Caterpillar(_grid);

                _scoreAndLives = new ScoreAndLives();

                _scoreAndLives.Lives = 3;
                _scoreAndLives.NoMoreLives += GameOver;

                _catepillar.ResetEvent += _scoreAndLives.CaterpillarDies;

                _grid.CheckSpace += _catepillar.OnCheck;

                _grid.AddNextTokens(_catepillar, new EventArgs());                

                ChangeDirection += _catepillar.ChangeDirection;

                _grid.PopupScore += _scoreAndLives.AddPointsAndLives;
            }



            return loadStatus;
        }

        public virtual void OnChangeDirection(ChangeDirectionEventArgs e)
        {
            ChangeDirection?.Invoke(this, e);
        }

        public char[,] Update()
        {
            char[,] map = new char[_grid.Width, _grid.Height];

            if (ShouldContinue)
            {                
                _catepillar.Update();
            }

            for (int i = 0; i < _grid.Width; i++)
            {
                for (int j = 0; j < _grid.Height; j++)
                {
                    if (_grid[i,j] is Wall)
                    {
                        map[i, j] = 'W';
                    }
                    else if (_grid[i, j] is Space)
                    {
                        map[i, j] = ' ';
                    }
                    else if (_grid[i, j] is CaterpillerShrinker)
                    {
                        map[i, j] = 'S';
                    }
                    else if (_grid[i, j] is CaterpillarGrower)
                    {
                        map[i, j] = 'G';
                    }
                }
            }

            foreach (var item in _catepillar.Points)
            {
                map[item.X, item.Y] = 'C';                
            }

            return map;
            
        }

        public void GameOver(object sender, EventArgs e)
        {
            ShouldContinue = false;
            DisplayScore?.Invoke(this, e);
        }

        public int GetScore()
        {
            return _scoreAndLives.Score;
        }
    }
}
